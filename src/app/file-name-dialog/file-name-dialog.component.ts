import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-file-name-dialog',
  templateUrl: './file-name-dialog.component.html',
  styleUrls: ['./file-name-dialog.component.css']
})
export class FileNameDialogComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<FileNameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      filename: this.data ? this.data.name : ''
    });
  }

  submit(form) {
    this.dialogRef.close(`${form.value.filename}`);
  }
}
