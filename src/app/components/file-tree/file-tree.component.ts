import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { FileNameDialogComponent } from '../../file-name-dialog/file-name-dialog.component';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-file-tree',
  templateUrl: './file-tree.component.html',
  styleUrls: ['./file-tree.component.css']
})
export class FileTreeComponent implements OnInit {

  files = [];
  fileNameDialogRef: MatDialogRef<FileNameDialogComponent>;
  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  openFileDialog(file?) {
    this.fileNameDialogRef = this.dialog.open(FileNameDialogComponent, {
      hasBackdrop: false,
      data: {
        filename: file ? file.name : ''
      }
    });

    this.fileNameDialogRef
      .afterClosed()
      .pipe(filter(name => name))
      .subscribe(name => {
        if(file) {
          const index = this.files.findIndex(f => f.name === file.name);
          if(index !== -1) {
            this.files[index] = {name, content: file.content}
          }
        } else {
          this.files.push({name, content: ''})
        }
      });
  }
}
