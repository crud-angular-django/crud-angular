import { Injectable } from '@angular/core';
import { Student } from './student';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  endpoint: string = 'http://localhost:8000/student/';
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  //errorMgmt: any = '';

  constructor(
    private http: HttpClient
  ) { }

  addStudent(data: Student): Observable<any> {
    let API_URL = `${this.endpoint}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.errorMgmt)
      )
  }

  getStudents() {
    return this.http.get(`${this.endpoint}`);
  }

  getStudent(id): Observable<any> {
    let API_URL = `${this.endpoint}${id}`;
    return this.http.get(API_URL, {headers: this.headers}).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.errorMgmt)
    )
  }

  UpdateStudent(data: Student): Observable<any> {
    let API_URL = `${this.endpoint}`;
    return this.http.put(API_URL, data, {headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }

  DeleteStudent(id): Observable<any> {
    let API_URL = `${this.endpoint}${id}`;
    return this.http.delete(API_URL).pipe(
      catchError(this.errorMgmt)
    )
  }

  // Error handling 
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
